#!/bin/bash

echo "SHUTDOWN TASK MANAGER CLIENT...";
kill -9 $(pgrep -f task-manager-client.jar)
echo "OK";
