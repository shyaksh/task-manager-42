package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class SessionService implements ISessionService {

    private static final String HEADER_COOKIE = "Cookie";

    private static final String HEADER_SET_COOKIE = "Set-Cookie";

    @Nullable
    private List<String> setCookieRow;

    @Override
    public void extractFrom(@NotNull final Object port) {
        setCookieRow = getListSetCookieRow(port);
    }

    @Override
    public void insertTo(@NotNull final Object port) {
        if (setCookieRow == null) throw new AccessDeniedException();
        setListCookieRowRequest(port, setCookieRow);
    }

    @Override
    public void clear() {
        setCookieRow = null;
    }

    @Nullable
    private List<HttpCookie> getListSetCookie(@NotNull final Object port) {
        @Nullable final List<String> listSetCookieRow = getListSetCookieRow(port);
        if (listSetCookieRow == null) return null;
        @NotNull final List<HttpCookie> result = new ArrayList<>();
        @Nullable final List<String> listCookie = getListSetCookieRow(port);
        if (listCookie == null) return null;
        for (@NotNull final String cookieRow : listCookie) {
            @NotNull final List<HttpCookie> httpCookies = HttpCookie.parse(cookieRow);
            @Nullable final HttpCookie httpCookie = httpCookies.get(0);
            result.add(httpCookie);
        }
        return result;
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private List<String> getListSetCookieRow(@NotNull final Object port) {
        @Nullable final Map<String, Object> httpResponseHeaders = getHttpResponseHeaders(port);
        if (httpResponseHeaders == null) return null;
        return (List<String>) httpResponseHeaders.get(HEADER_SET_COOKIE);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private Map<String, Object> getHttpResponseHeaders(@NotNull final Object port) {
        @Nullable final Map<String, Object> responseContext = getResponseContext(port);
        if (responseContext == null) return null;
        return (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
    }

    @Nullable
    private Map<String, Object> getResponseContext(@Nullable final Object port) {
        BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getResponseContext();
    }

    @Nullable
    private BindingProvider getBindingProvider(@Nullable final Object port) {
        if (port == null) return null;
        return (BindingProvider) port;
    }

    private void setListCookieRowRequest(@NotNull final Object port, @NotNull final List<String> value) {
        @Nullable final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext != null && getHttpRequestHeaders(port) == null) {
            requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<String, Object>());
        }
        @Nullable final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(HEADER_COOKIE, value);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private Map<String, Object> getHttpRequestHeaders(@NotNull final Object port) {
        @Nullable final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    @Nullable
    private Map<String, Object> getRequestContext(@Nullable final Object port) {
        @Nullable final BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getRequestContext();
    }

}
