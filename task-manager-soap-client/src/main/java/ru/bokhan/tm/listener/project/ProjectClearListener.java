package ru.bokhan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.ProjectEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class ProjectClearListener extends AbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionService.insertTo(projectEndpoint);
        System.out.println("[CLEAR PROJECTS]");
        projectEndpoint.deleteProjectAll();
        System.out.println("[OK]");
    }

}
