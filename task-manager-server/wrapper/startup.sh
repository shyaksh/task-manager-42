#!/bin/bash
PORT="8090"
if [ -n "$1" ]; then
  PORT=$1
fi

FILENAME="tm-server-$PORT"
echo "STARTING TASK MANAGER SERVER AT PORT: $PORT..."
if [ -f ./"$FILENAME".pid ]; then
  echo "Application already started!"
  exit 1
fi

mkdir -p ../log
rm -f ../log/"$FILENAME".log
nohup java -Dserver.port="$PORT" -jar ./task-manager-server.jar >../log/"$FILENAME".log 2>&1 &
echo $! >"$FILENAME".pid
echo "OK"
