package ru.bokhan.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class Domain implements Serializable {

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private List<TaskDTO> tasks = new ArrayList<>();

    @NotNull
    private List<UserDTO> users = new ArrayList<>();

}
