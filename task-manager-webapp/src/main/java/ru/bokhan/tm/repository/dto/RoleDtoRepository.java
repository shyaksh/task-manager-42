package ru.bokhan.tm.repository.dto;

import ru.bokhan.tm.dto.RoleDto;

public interface RoleDtoRepository extends AbstractDtoRepository<RoleDto> {
}
