package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bokhan.tm.api.service.IRoleService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.dto.RoleDto;
import ru.bokhan.tm.dto.UserDto;
import ru.bokhan.tm.entity.Role;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.RoleType;
import ru.bokhan.tm.exception.empty.*;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.repository.dto.UserDtoRepository;
import ru.bokhan.tm.repository.entity.UserRepository;
import ru.bokhan.tm.util.HashUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean extends AbstractService<UserDto, User> implements UserDetailsService, IUserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserDtoRepository dtoRepository;

    @Autowired
    private IRoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = repository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : user.getRoles()) roles.add(role.toString());
        return new CustomUser(
                org.springframework.security.core.userdetails.User
                        .withUsername(user.getLogin())
                        .password(user.getPasswordHash())
                        .roles(roles.toArray(new String[]{}))
                        .build()
        ).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if (repository.count() > 0) return;
        create("test", "test");
        createWithRole("admin", "admin", RoleType.ADMINISTRATOR);
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password) {
        if (login == null) return;
        if (password == null) return;
        @NotNull final User user = new User();
        @NotNull final RoleDto role = roleService.createWithoutSaving(user.getId());
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        repository.save(user);
        roleService.save(role);
    }

    @Override
    public void create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        save(user);
    }

    @Override
    public void createWithRole(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null) return;
        if (password == null) return;
        if (roleType == null) return;

        @NotNull final UserDto user = new UserDto();
        @NotNull final RoleDto role =
                roleService.createByUserIdAndTypeWithoutSaving(user.getId(), roleType);
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        dtoRepository.save(user);
        roleService.save(role);
    }

    @Nullable
    @Override
    public UserDto findById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        return dtoRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null) throw new EmptyLoginException();
        return dtoRepository.findByLogin(login);
    }

    @Override
    public void deleteById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        repository.deleteById(id);
    }

    @Override
    public void deleteByLogin(@Nullable final String login) {
        if (login == null) throw new EmptyLoginException();
        repository.deleteByLogin(login);
    }

    @Override
    public void updateById(
            @Nullable final String id,
            @Nullable final String login,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName,
            @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final UserDto userDto = findById(id);
        if (userDto == null) throw new IncorrectIdException();
        userDto.setLogin(login);
        userDto.setFirstName(firstName);
        userDto.setMiddleName(middleName);
        userDto.setLastName(lastName);
        userDto.setEmail(email);
        save(userDto);
    }

    @Override
    public void updatePasswordById(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDto user = findById(id);
        if (user == null) throw new IncorrectIdException();
        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        save(user);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        save(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        save(user);
    }

}
