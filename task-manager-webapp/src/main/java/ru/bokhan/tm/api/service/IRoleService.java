package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.RoleDto;
import ru.bokhan.tm.entity.Role;
import ru.bokhan.tm.enumerated.RoleType;

public interface IRoleService extends IService<RoleDto, Role> {

    RoleDto createWithoutSaving(@Nullable String userId);

    RoleDto createByUserIdAndTypeWithoutSaving(
            @Nullable String userId,
            @NotNull RoleType roleType
    );

    @Nullable
    RoleDto findById(@Nullable String id);

    void deleteById(@Nullable String id);

}
