package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<ProjectDto, Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable ProjectDto projectDto);

    void deleteAllByUserId(@Nullable String userId);

    @NotNull
    List<ProjectDto> findAllByUserId(@Nullable String userId);

    @Nullable
    ProjectDto findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDto findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    ProjectDto findByName(@Nullable String userId, @Nullable String name);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    void deleteByIndex(@Nullable String userId, @Nullable Integer index);

    void deleteByName(@Nullable String userId, @Nullable String name);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}