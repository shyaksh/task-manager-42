package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<TaskDto, Task> {

    void create(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name);

    void create(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name,
            String description
    );

    void add(@Nullable String userId, @Nullable TaskDto taskDto);

    @NotNull
    List<TaskDto> findAllByUserId(@Nullable String userId);

    void deleteAllByUserId(@Nullable String userId);

    @Nullable
    TaskDto findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDto findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    TaskDto findByName(@Nullable String userId, @Nullable String name);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    void deleteByIndex(@Nullable String userId, @Nullable Integer index);

    void deleteByName(@Nullable String userId, @Nullable String name);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}