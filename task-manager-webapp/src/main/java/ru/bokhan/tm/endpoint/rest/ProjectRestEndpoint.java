package ru.bokhan.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.IProjectRestEndpoint;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.util.SecurityUtil;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Nullable
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto save(@NotNull @RequestBody final ProjectDto projectDto) {
        projectDto.setUserId(SecurityUtil.getUserId());
        return projectService.save(projectDto);
    }

    @Nullable
    @Override
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @Override
    @GetMapping(value = "/exists/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectService.deleteByUserIdAndId(SecurityUtil.getUserId(), id);
    }

}
