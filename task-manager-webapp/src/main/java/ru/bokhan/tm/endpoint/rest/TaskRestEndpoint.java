package ru.bokhan.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.ITaskRestEndpoint;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.util.SecurityUtil;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Nullable
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public TaskDto save(@NotNull @RequestBody final TaskDto taskDto) {
        taskDto.setUserId(SecurityUtil.getUserId());
        return taskService.save(taskDto);
    }

    @Nullable
    @Override
    @GetMapping("/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskService.findByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskService.existsByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        taskService.deleteByUserIdAndId(SecurityUtil.getUserId(), id);
    }

}
