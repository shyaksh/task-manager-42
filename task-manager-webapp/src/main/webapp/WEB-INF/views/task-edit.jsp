<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK EDIT</h1>
<form:form action="/task/edit/${task.id}/" method="post" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <p>
    <div>NAME:</div>
    <div><form:input type="text" path="name"/></div>
    <p>
    <div>DESCRIPTION:</div>
    <div><form:input type="text" path="description"/></div>
    <p>
    <div>STATUS</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    <p>
    <div>PROJECT</div>
    <div>
        <form:select path="projectId">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </div>
    <p>
    <div>DATE START:</div>
    <div><form:input type="date" path="dateStart"/></div>
    <p>
    <div>DATE FINISH:</div>
    <div><form:input type="date" path="dateFinish"/></div>
    <button type="submit" style="margin-top: 20px">SAVE TASK</button>
</form:form>
<jsp:include page="../include/_footer.jsp"/>
